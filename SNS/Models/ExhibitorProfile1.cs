﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SNS.Models
{
    public class ExhibitorProfile1
    {
        public int Id { get; set; }
        public string USC { get; set; }
        public string TheatreName { get; set; }
        public string Address { get; set; }
        public string TheatreType { get; set; }
        public int ScreenCount { get; set; }
        public int StaffStrength { get; set; }
        public string FandbBusiness { get; set; }
        public string OtherAmenities { get; set; }
        
        public string ContactNumber { get; set; }
        public string Email { get; set; }
        public bool OwnershipProprietary { get; set; }
        public bool OwnershipPartnership { get; set; }
        public bool OwnershipCompany { get; set; }


    }
}