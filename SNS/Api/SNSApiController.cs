﻿using SNS.ApiModels;
using SNS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
namespace SNS.Api
{
    public class SNSApiController : ApiController
    {
        ApplicationDbContext db;
        public SNSApiController()
        {
            db = new ApplicationDbContext();
        }

        [HttpPost]
        public IHttpActionResult GenerateOTP(OTPModel model)
        {
            string response = "Success";
            try
            {
                var record = db.Customers.Where(x => x.Email.Equals(model.Email) && x.Category.Equals(model.Category)).SingleOrDefault();
                if (record != null && record.Email.ToLower() == model.Email.ToLower())
                {
                    OTP dmo = new OTP();
                    dmo.Category = model.Category;
                    dmo.OTPString = SNS.Common.Utility.GenerateOTP();
                    dmo.ExpiryIn = WebConfigurationManager.AppSettings["OTPExpiryTime"].ToString();
                    dmo.EmailID = record.Email;
                    db.OTP.Add(dmo);
                }
                else
                {
                    response = "Email Not found";
                }
            }
            catch (Exception ex)
            {
                response = "Error";
            }
            return Json(new { data = response });
        }
    }
}
