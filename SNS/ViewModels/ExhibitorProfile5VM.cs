﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SNS.Models;

namespace SNS.ViewModels
{
    public class ExhibitorProfile5VM

    {
        public int Id { get; set; }

        [Required(ErrorMessage = "*")]
        [DisplayName("USC : ")]
        public string USC { get; set; }

        [DisplayName("Genres : ")]
        public string Genres { get; set; }

        [DisplayName("Best Release From : ")]
        public DateTime BestReleaseFrom { get; set; }

        [DisplayName("Best Release To : ")]
        public DateTime BestReleaseTo { get; set; }

        [DisplayName("Worst Release From : ")]
        public DateTime WorstReleaseFrom { get; set; }

        [DisplayName("Worst Release To : ")]
        public DateTime WorstReleaseTo { get; set; }

        [DisplayName("Successful Stars : ")]
        public string SuccessfulStars { get; set; }

        [DisplayName("Regional Interest : ")]
        public string RegionalInterest { get; set; }

        [DisplayName("Regional Profitability : ")]
        public string RegionalProfitability { get; set; }

        [DisplayName("Regional Distribution : ")]
        public string RegionalDistribution { get; set; }

        [DisplayName("English Interest : ")]
        public string EnglishInterest { get; set; }

        [DisplayName("English Profitability : ")]
        public string EnglishProfitability { get; set; }

        [DisplayName("English Distribution : ")]
        public string EnglishDistribution { get; set; }

        [DisplayName("Dubbed Interest : ")]
        public string DubbedInterest { get; set; }

        [DisplayName("Dubbed Profitability : ")]
        public string DubbedProfitability { get; set; }

        [DisplayName("Dubbed Distribution : ")]
        public string DubbedDistribution { get; set; }

        [DisplayName("Unique Case : ")]
        public string UniqueCase { get; set; }

        [DisplayName("Unique Flop : ")]
        public string UniqueFlop { get; set; }

        [DisplayName("Business Existing Challenges : ")]
        public string BusinessExistingChallenges { get; set; }

        [DisplayName("Business Potential Solutions : ")]
        public string BusinessPotentialSolutions { get; set; }

        [DisplayName("Future Existing Challenges : ")]
        public string FutureExistingChallenges { get; set; }

        [DisplayName("Future Potential Solutions : ")]
        public string FuturePotentialSolutions { get; set; }
        

    }
}