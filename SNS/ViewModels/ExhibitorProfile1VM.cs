﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;


namespace SNS.ViewModels
{
    public class ExhibitorProfile1VM
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "*")]
        [DisplayName("USC : ")]                 
        public string USC { get; set; }

        [DisplayName("Theatre Name : ")]
        public string TheatreName { get; set; }

        [DisplayName("Address : ")]
        public string Address { get; set; }

        [DisplayName("Theatre Type : ")]
        public string TheatreType { get; set; }

        [DisplayName("Screen Count : ")]
        public int ScreenCount { get; set; }

        [DisplayName("Staff Strength : ")]
        public int StaffStrength { get; set; }

        [DisplayName("FandbBusiness : ")]
        public string FandbBusiness { get; set; }

        [DisplayName("Other Amenities : ")]
        public string OtherAmenities { get; set; }

        

        [DisplayName("Contact Number : ")]
        public string ContactNumber { get; set; }

        [DisplayName("Email : ")]
        public string Email { get; set; }

        [DisplayName("Ownership-Proprietary : ")]
        public bool OwnershipProprietary { get; set; }
        [DisplayName("Ownership-Partnership : ")]
        public bool OwnershipPartnership { get; set; }
        [DisplayName("Ownership-Company : ")]
        public bool OwnershipCompany { get; set; }

    }
}