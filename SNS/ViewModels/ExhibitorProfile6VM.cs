﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SNS.Models;

namespace SNS.ViewModels
{
    public class ExhibitorProfile6VM
    
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "*")]
        [DisplayName("USC : ")]
        public string USC { get; set; }

        [DisplayName("Publicity Expenses : ")]
        public string PublicityExpenses { get; set; }

        [DisplayName("Publicity Property : ")]
        public string PublicityProperty { get; set; }

        [DisplayName("Poster Sourcing : ")]
        public string PosterSourcing { get; set; }

        [DisplayName("Poster Cost Whom : ")]
        public string PosterCostWhom { get; set; }

        [DisplayName("Distributor Challenges : ")]
        public string DistributorChallenges { get; set; }

        [DisplayName("Distributor Practises : ")]
        public string Distributorpractises { get; set; }

        [DisplayName("Producer Challenges : ")]
        public string ProducerChallenges { get; set; }

        [DisplayName("Producer Deals : ")]
        public string ProducerDeals { get; set; }

        [DisplayName("Occupancy Challenges : ")]
        public string OccupancyChallenges { get; set; }

        [DisplayName("Occupancy Support : ")]
        public string OccupancySupport { get; set; }
        
    }
}