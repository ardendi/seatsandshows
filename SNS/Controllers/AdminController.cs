﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SNS.ApiModels;
using SNS.Common;
using SNS.Models;
using SNS.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Net.Http;

namespace SNS.Controllers
{
    public class AdminController : Controller
    {
        ApplicationDbContext db;
        ApplicationDbContext context;
        public AdminController()
        {
            db = new ApplicationDbContext();
            context = new ApplicationDbContext();
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MainLogin()
        {
            ViewBag.Category = new List<string> { "Exhibitor", "Producer", "Enterperenuer" };
            return View("MainLogin");
        }

        public ActionResult Login1()
        {
            ViewBag.Category = new List<string> { "Exhibitor", "Producer", "Enterperenuer" };
            return View("MainLogin");
        }

        public ActionResult AdminLogin()
        {
            return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public ActionResult Login()
        {
            return RedirectToAction("Dashboard", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LoginExhibitor([Bind(Include = "Id,USC,Username,Password")] CustomersVM cust)
        {
            try
            {
                var dmo = Mapper.Map<CustomersVM, Customers>(cust);
                Customers customers = db.Customers.Where(i => i.Username == dmo.Username).Single();
                if (customers == null)
                {
                    return HttpNotFound();
                }
                else
                {
                    if(dmo.Password == customers.Password)
                    {
                        return this.RedirectToAction("Index","Exhibitor", new { usc = customers.USC });

                        //TempData["USC"] = customers.USC;
                        //return new RedirectResult(@"~\Exhibitor\");
                        //return RedirectToAction("Index", "Exhibitor", new { usc = dmo.USC });
                    }
                }
                return RedirectToAction("Index","User");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index", "User");
            }
        }

        public ActionResult SalesLogin()
        {
            return View();
        }
        public ActionResult AddSales()
        {
            SalesAgentVM vm = new SalesAgentVM();
            return View("AddSalesAgent", vm);
        }
        public ActionResult Producer()
        {
            return View("Producer");
        }
        public ActionResult Entreprenuer()
        {
            return View("Entreprenuer");
        }
        public ActionResult Exhibitor()
        {
            return View("Exhibitor");
        }
        public ActionResult Cancel()
        {
            return RedirectToAction("SalesAgent", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddSalesAgent([Bind(Include = "Id,EmailId,Mobile,Password,Name,ConfirmPassword")] SalesAgentVM salesAgent)
        {
            if (ModelState.IsValid)
            {
                var dmo = Mapper.Map<SalesAgentVM, SalesAgent>(salesAgent);
                if (dmo.Id == 0)
                {
                    dmo.CreatedOn = DateTime.Now;
                    dmo.UpdatedOn = DateTime.Now;
                    db.SalesAgent.Add(dmo);
                    db.SaveChanges();
                    RegisterViewModel vm = new Models.RegisterViewModel();
                    vm.Email = dmo.EmailId;
                    vm.Password = dmo.Password;
                    vm.ConfirmPassword = dmo.Password;
                    vm.UserName = dmo.Name;
                    vm.UserRoles = "Salesagent";
                    var user = new ApplicationUser { UserName = dmo.Name, Email = dmo.EmailId };
                    var result = await UserManager.CreateAsync(user, dmo.Password);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                        // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                        // Send an email with this link
                        // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                        // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
                        //Assign Role to user Here   
                        await this.UserManager.AddToRoleAsync(user.Id, "Saleagent");
                        //Ends Here 
                        return RedirectToAction("SalesAgent", "Home");
                    }
                    ViewBag.Name = new SelectList(context.Roles.Where(u => !u.Name.Contains("Admin"))
                                              .ToList(), "Name", "Name");
                    AddErrors(result);

                }
                else
                {
                    dmo.CreatedOn = DateTime.Now;
                    dmo.UpdatedOn = DateTime.Now;
                    db.Entry(dmo).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return RedirectToAction("SalesAgent", "Home");
            }

            return View(salesAgent);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

       public ActionResult FindUSC(int? id)
       {
            
           if (id.Equals(null))
           {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
           }

         Customers customers = db.Customers.Find(id);
         var dmo = Mapper.Map<Customers, CustomersVM>(customers);
         if (customers == null)
         {
             return HttpNotFound();
         }

           return View("Index",dmo);

       }

        public ActionResult EditAgent(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            SalesAgent salesAgent = db.SalesAgent.Find(id);
            var dmo = Mapper.Map<SalesAgent, SalesAgentVM>(salesAgent);
            if (salesAgent == null)
            {
                return HttpNotFound();
            }
            return View("AddSalesAgent", dmo);
        }

        public ActionResult DeleteAgent()
        {
            int id = Convert.ToInt32(Request.QueryString["idQS"]);
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SalesAgent salesAgent = db.SalesAgent.Find(id);
            if (salesAgent == null)
            {
                return HttpNotFound();
            }
            else
            {
                try
                {
                    db.SalesAgent.Remove(salesAgent);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return View("Dashboard", "Home");
                }
            }
            return RedirectToAction("SalesAgent", "Home");
        }

        //public ActionResult uscInsertedCode(string? usc)
        //{
        //if (usc == null)
        //{
        //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //}

        //SalesAgent salesAgent = db.SalesAgent.Find(id);
        //var dmo = Mapper.Map<SalesAgent, SalesAgentVM>(salesAgent);
        //if (salesAgent == null)
        //{
        //    return HttpNotFound();
        //}
        //   return View("AddSalesAgent", dmo);
        //}

        //public async Task<ActionResult> UpdateSalesAgent(RegisterViewModel model)
        //{
        //    var user = new ApplicationUser { UserName = model.UserName, Email = model.Email };
        //    var result = await UserManager.CreateAsync(user, model.Password);
        //    if (result.Succeeded)
        //    {
        //        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

        //        // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
        //        // Send an email with this link
        //        // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
        //        // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
        //        // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
        //        //Assign Role to user Here   
        //        await this.UserManager.AddToRoleAsync(user.Id, model.UserRoles);
        //        //Ends Here 
        //        return RedirectToAction("SalesAgent", "Home");
        //    }
        //    ViewBag.Name = new SelectList(context.Roles.Where(u => !u.Name.Contains("Admin"))
        //                                   .ToList(), "Name", "Name");
        //    AddErrors(result);

        //}
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AdminController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        [HttpPost]
        public JsonResult VerifyAndGenerateOTP(OTPModel model,FormCollection formCollection)
        {
            //Request.QueryString["usc"]
            string inputUSC = (string)formCollection["USCHidden"];
            var customer = db.Customers.Where(i => i.USC == inputUSC).Single();
            string dbEmail = customer.Email;
            string inputEmail = (string)formCollection["EmailID"];

            string response = "Success";
            string msgBody = "";
            if (inputEmail == dbEmail)
            {
                try
                {
                        OTP dmo = new OTP();
                        dmo.Category = customer.Category;
                        dmo.OTPString = SNS.Common.Utility.GenerateOTP();
                        dmo.ExpiryIn = WebConfigurationManager.AppSettings["OTPExpiryTime"].ToString();
                        dmo.EmailID = customer.Email;
                        dmo.CreatedOn = DateTime.UtcNow;
                        db.OTP.Add(dmo);
                        db.SaveChanges();
                        msgBody = "Received OTP Code is :" + dmo.OTPString;
                        Utility.SendMail(msgBody, dmo.EmailID, "One Time Password from S & S");
                   
                }
                catch (Exception ex)
                {
                    response = "Error";
                }
                return Json(new { data = response });
            }
            else
            {
                response = "Invalid Email address!";
                return Json(new { data=response});
            }
        }

        public ActionResult VerifyOTP(OTPModel model)
        {
            var expiryTime = DateTime.UtcNow.AddMinutes(Convert.ToInt32(WebConfigurationManager.AppSettings["OTPExpiryTime"]));

            var record = db.OTP.Where(x => x.OTPString.Equals(model.Password)).SingleOrDefault();
            if (record != null && DateTime.UtcNow <= expiryTime && record.OTPString == model.Password)
            {
                TempData["Category"] = record.Category;
                TempData["EmailID"] = record.EmailID;
                return RedirectToAction("USCScreen"); 
            }
            ModelState.AddModelError(string.Empty, "Invalid");
            return View("MainLogin");
        }
        [HttpPost]
        public ActionResult GetScreen(string USC)
        {
            return RedirectToAction("AboutTheatre", "OnBoarding");
        }
        public ActionResult USCScreen()
        {
            //ViewBag.Category = (string)TempData.Peek("Category");
            //ViewBag.EmailID = (string)TempData.Peek("EmailID");
            //TempData.Keep("Category");
            //TempData.Keep("EmailID");
            return View();
        }
        
        [HttpPost]
        public ActionResult VerifyCustomer(FormCollection formCollection,string usc)
        {
            try
            {
                if (usc != null)
                {
                    Customers customer = db.Customers.Where(i => i.USC == usc).Single();
                    var dmo = Mapper.Map<Customers, CustomersVM>(customer);

                    if (customer!=null)
                    {
                        return PartialView("_validateUSC", dmo);
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                return Json(new { data = "asdf" });

            }
            return Json(new { data = "asdf" });

        }

        [HttpPost]
        public JsonResult GenerateOTPnow(FormCollection formCollection, OTPModel model)//,string inputEmail,string dbEmail)
        {
            string inputEmail = (string)formCollection["txtEmail"];
            string dbEmail = (string)formCollection["txtDbEmail"];

            string response = "";
            string msgBody = "";



            if (inputEmail == dbEmail)
            {
                try
                {
                    OTP dmo = new OTP();
                    dmo.Category = "Exhibitor";
                    dmo.OTPString = SNS.Common.Utility.GenerateOTP();
                    response += dmo.OTPString;
                    dmo.ExpiryIn = WebConfigurationManager.AppSettings["OTPExpiryTime"].ToString();
                    dmo.EmailID = inputEmail;
                    dmo.CreatedOn = DateTime.UtcNow;
                    db.OTP.Add(dmo);
                    db.SaveChanges();
                    msgBody = "Received OTP Code is :" + dmo.OTPString;
                    Utility.SendMail(msgBody, dmo.EmailID, "One Time Password from S & S");
                }
                catch (Exception ex)
                {
                    response = "Error";
                }
            }
            return Json(new { data = response });
        }
        [HttpPost]
        public ActionResult VerifyOTPnow(FormCollection formCollection, OTPModel model,string inputOTP,string generatedOTP, string dbUSC)
        {

            string response = "";
            
            if (inputOTP != null && generatedOTP != null && inputOTP == generatedOTP)
            {
                try
                {
                    Customers customers = db.Customers.Where(i => i.USC == dbUSC).Single();
                    var dmo = Mapper.Map<Customers, CustomersVM>(customers);
                    return PartialView("_createLoginCredentials",dmo);
                }
                catch (Exception ex)
                {
                    response = "Invalid OTP";
                    return RedirectToAction(this.Request.UrlReferrer.AbsolutePath);
                }
            }
            return RedirectToAction(this.Request.UrlReferrer.AbsolutePath);
        }

        [HttpPost]
        public ActionResult SaveCredentials(FormCollection formCollection)
        {
            string usc = (string)formCollection["txtUSC"];
            string username = (string)formCollection["txtUsername"];
            string password = (string)formCollection["txtPassword"];
            string confirmPassword = (string)formCollection["txtConfirmPassword"];

            if(usc != null && username !=null && password != null && confirmPassword != null)
            {
                if(password == confirmPassword)
                {
                    Customers customers = db.Customers.Where(i => i.USC == usc).Single();
                    customers.Username = username;
                    customers.Password = password;
                    customers.ConfirmPassword = confirmPassword;
                    db.Entry(customers).State = EntityState.Modified;
                    //db.Customers.Add(dmo);
                    db.SaveChanges();
                    return RedirectToAction("Index", "User");
                }
                else
                {
                    return View();
                }
            }
            return View();
        }

        [HttpPost]
        public ActionResult SetScreen(FormCollection formCollection)
        {
            string category = (string)formCollection["Category"];
            string emailid = (string)formCollection["EmailID"];
            var record = db.Customers.Where(x => x.Email.Equals(emailid) && x.Category.Equals(category)).SingleOrDefault();
            if (record != null && record.Email.Length > 0 && record.USC.Equals(formCollection["USC"]))
            {
                return RedirectToAction("AboutTheatre", "OnBoarding");
            }
            TempData["Category"] = category;
            TempData["EmailID"] = emailid;
            ModelState.AddModelError(string.Empty, "Invalid");
            return RedirectToAction("USCScreen");
        }
        [HttpPost]
        public ActionResult CustomerLogin(FormCollection formCollection)
        {
            //    var result = db.Customers.Where(x=>x)
            return RedirectToAction("Index", "Customer");
        }

        [HttpPost]
        public ActionResult ExhibitorLogin(FormCollection formCollection)
        {
            //    var result = db.Customers.Where(x=>x)
            return RedirectToAction("Index", "Exhibitor");
        }

        public ActionResult FindUSCLogin(FormCollection formCollection)
        {
            //    var result = db.Customers.Where(x=>x)
            return View("FindUSC", "Admin");
        }

    }
}