﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SNS.Models
{
    public class Customers
    {
        public int Id { get; set; }
        public string TheatreName { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        public string USC { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string TheatreType { get; set; }
        public string ScreenCount { get; set; }
        public string Category { get; set; }
        public string TicketPrice { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

    }
}