﻿using AutoMapper;
using SNS.Models;
using SNS.ViewModels;

namespace SNS.AutoMapping
{
    public class ViewModelToDomainMappingProfile : Profile
    {

        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        protected override void Configure()
        {
            CreateMap<SalesAgentVM, SalesAgent>();
            CreateMap<CustomersVM, Customers>();
            CreateMap<ExhibitorProfile1VM, ExhibitorProfile1>();
            CreateMap<ExhibitorProfile2VM, ExhibitorProfile2>();
            CreateMap<ExhibitorProfile3VM, ExhibitorProfile3>();
            CreateMap<ExhibitorProfile4VM, ExhibitorProfile4>();
            CreateMap<ExhibitorProfile5VM, ExhibitorProfile5>();
            CreateMap<ExhibitorProfile6VM, ExhibitorProfile6>();

        }


    }

}








