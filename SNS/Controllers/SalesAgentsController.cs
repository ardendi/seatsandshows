﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using SNS;
using SNS.Models;
using SNS.ViewModels;

namespace SNS.Controllers
{
    public class SalesAgentsController : Controller
    {

        ApplicationDbContext db;
        public SalesAgentsController()
        {
            db = new ApplicationDbContext();
        }       

        // GET: SalesAgents
        public ActionResult Index()
        {
            return View(db.SalesAgent.ToList());
        }
        public ActionResult GetGridData()
        {
            return Json(new  { Data = db.SalesAgent.ToList() }, JsonRequestBehavior.AllowGet);
           // return Json(db.SalesAgent.ToList());
        }

        // GET: SalesAgents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SalesAgent salesAgent = db.SalesAgent.Find(id);
            if (salesAgent == null)
            {
                return HttpNotFound();
            }
            return View(salesAgent);
        }

        // GET: SalesAgents/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SalesAgents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,EmailId,Mobile,Password")] SalesAgentVM salesAgent)
        {
            if (ModelState.IsValid)
            {
                var dmo = Mapper.Map<SalesAgentVM, SalesAgent>(salesAgent);
                dmo.CreatedOn = DateTime.Now;
                dmo.UpdatedOn = DateTime.Now;
                db.SalesAgent.Add(dmo);
                db.SaveChanges();
                RegisterViewModel vm = new Models.RegisterViewModel();
                vm.Email = dmo.EmailId;
                vm.Password = dmo.Password;
                vm.ConfirmPassword = dmo.Password;
                vm.UserName = dmo.Name;
                vm.UserRoles = "Salesagent";
                return RedirectToAction("UpdateSalesAgent", "Account", vm);
            }

            return RedirectToAction("Admin", "AddSalesAgent", salesAgent);
        }

        // GET: SalesAgents/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SalesAgent salesAgent = db.SalesAgent.Find(id);
            if (salesAgent == null)
            {
                return HttpNotFound();
            }
            return View(salesAgent);
        }

        // POST: SalesAgents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,EmailId,Mobile,Password")] SalesAgent salesAgent)
        {
            if (ModelState.IsValid)
            {
                db.Entry(salesAgent).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(salesAgent);
        }

        // GET: SalesAgents/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SalesAgent salesAgent = db.SalesAgent.Find(id);
            if (salesAgent == null)
            {
                return HttpNotFound();
            }
            return View(salesAgent);
        }

        // POST: SalesAgents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SalesAgent salesAgent = db.SalesAgent.Find(id);
            db.SalesAgent.Remove(salesAgent);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
