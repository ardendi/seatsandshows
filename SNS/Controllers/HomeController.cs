﻿using SNS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SNS.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
        }
        ApplicationDbContext db;
        public HomeController()
        {
            db = new ApplicationDbContext();
        }

        public ActionResult Dashboard()
        {
            //var result = db.Customers.GroupBy(a => a.Category).Select(g => new { Category = g.Key, Count = g.Count() });
            ViewBag.Exhibitor = db.Customers.Where(a => a.Category == "Exhibitor").Count();//result.ToList().Where(x => x.Category.Equals("Exhibitor")).Count();
            ViewBag.Producer = db.Customers.Where(a => a.Category == "Producer").Count(); //result.ToList().Where(x => x.Category.Equals("Producer")).Count();
            ViewBag.SalesAgent = db.SalesAgent.Select(a=>a.Id).Count(); //result.Where(x => x.Category.Equals("Exhibitor")).Count();
            ViewBag.Enterprenuer = db.Customers.Where(a => a.Category == "Enterprenuer").Count(); //result.ToList().Where(x => x.Category.Equals("Enterprenuer")).Count();
            return View();
        }

        public ActionResult Customer()
        {
            return View();
        }
        public ActionResult SalesAgent()
        {
            return View();
        }

        public ActionResult About()
		{
			ViewBag.Message = "Your application description page.";

			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}
	}
}