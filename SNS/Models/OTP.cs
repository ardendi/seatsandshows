﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SNS.Models
{
    public class OTP
    {
        public int Id { get; set; }
        public string OTPString { get; set; }
        public string ExpiryIn { get; set; }
        public string EmailID { get; set; }
        public string Category { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}