﻿using AutoMapper;
using SNS.Models;
using SNS.ViewModels;

namespace SNS.AutoMapping
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        protected override void Configure()
        {

            CreateMap<SalesAgent, SalesAgentVM>();
            CreateMap<Customers, CustomersVM>();
            CreateMap<ExhibitorProfile1, ExhibitorProfile1VM>();
            CreateMap<ExhibitorProfile2, ExhibitorProfile2VM>();
            CreateMap<ExhibitorProfile3, ExhibitorProfile3VM>();
            CreateMap<ExhibitorProfile4, ExhibitorProfile4VM>();
            CreateMap<ExhibitorProfile5, ExhibitorProfile5VM>();
            CreateMap<ExhibitorProfile6, ExhibitorProfile6VM>();
        }

    }
}