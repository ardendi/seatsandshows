﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SNS.Models
{
    public class ExhibitorProfile3

    {

        public int Id { get; set; }
        public string USC { get; set; }
        public string FandbRevenue { get; set; }
        public string ParkingRevenue { get; set; }
        public string LocalAdvertisingRevenue { get; set; }
        public string OtherRevenue { get; set; }
        public string FandbBusiness { get; set; }
        public string AdditionalRevenue { get; set; }
        public string EffectiveTaxRate { get; set; }
        public string Avg3YrRevenue { get; set; }
        

    }
}