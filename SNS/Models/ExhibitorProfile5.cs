﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SNS.Models
{
    public class ExhibitorProfile5
    {

        public int Id { get; set; }
        public string USC { get; set; }
        public string Genres { get; set; }
        public DateTime BestReleaseFrom { get; set; }
        public DateTime BestReleaseTo { get; set; }
        public DateTime WorstReleaseFrom { get; set; }
        public DateTime WorstReleaseTo { get; set; }
        public string SuccessfulStars { get; set; }
        public string RegionalInterest { get; set; }
        public string RegionalProfitability { get; set; }
        public string RegionalDistribution { get; set; }
        public string EnglishInterest { get; set; }
        public string EnglishProfitability { get; set; }
        public string EnglishDistribution { get; set; }
        public string DubbedInterest { get; set; }
        public string DubbedProfitability { get; set; }
        public string DubbedDistribution { get; set; }
        public string UniqueCase { get; set; }
        public string UniqueFlop { get; set; }
        public string BusinessExistingChallenges { get; set; }
        public string BusinessPotentialSolutions { get; set; }
        public string FutureExistingChallenges { get; set; }
        public string FuturePotentialSolutions { get; set; }

    }
}