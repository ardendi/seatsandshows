﻿using AutoMapper;
using SNS.Models;
using SNS.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using SNS.Common;
using System.Web.Mvc;

namespace SNS.Controllers
{
    public class OnBoardingController : Controller
    {
        ApplicationDbContext db;
        public OnBoardingController()
        {
            db = new ApplicationDbContext();
        }
        // GET: Customers
        public ActionResult Index()
        {
            return View(db.Customers.ToList());
        }
        public ActionResult GetGridData()
        {
            return Json(new { Data = db.Customers.ToList() }, JsonRequestBehavior.AllowGet);
            // return Json(db.SalesAgent.ToList());
        }
        // GET: Customers/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customers exhibitor = db.Customers.Find(id);
            if (exhibitor == null)
            {
                return HttpNotFound();
            }
            return View(exhibitor);
        }

        // GET: Customers/Create
        public ActionResult Create()
        {
            Random rand = new Random((int)DateTime.Now.Ticks);

            string category = "";
            string username = "user" + rand.Next(1, 100);
            string password = "123456";
            if (Request.QueryString["Category"] != null)
            {
                category = Request.QueryString["Category"];
            }
            ViewBag.Category = category;
            ViewBag.Username = username;
            ViewBag.Password = password;

            CustomersVM dto = new CustomersVM();
            dto.Category = category;
            dto.Username = username;
            dto.Password = password;
            return View(dto);
        }

        // POST: Customers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        // USC Code Generates here
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TheatreName,Address,Country,State,City,Zip,TheatreType,ScreenCount,TicketPrice,Email,Mobile,Category,Username,Password")] CustomersVM exhibitor)
        {
            Random rand = new Random();
            var errors = ModelState.Values.SelectMany(v => v.Errors);
            try
            {
                if (ModelState.IsValid)
                {
                    var dmo = Mapper.Map<CustomersVM, Customers>(exhibitor);
                    dmo.CreatedOn = DateTime.Now;
                    dmo.UpdatedOn = DateTime.Now;
                    dmo.USC = Utility.GetThreeDigitCode(exhibitor.TheatreName) + "-" + Utility.GetThreeDigitCode(exhibitor.State) + "-" + Utility.GetThreeDigitCode(exhibitor.City) + "-" + exhibitor.Zip + "-" + rand.Next(1, 1000) + "S";
                    db.Customers.Add(dmo);
                    db.SaveChanges();
                    ExhibitorProfile1 expr1 = new ExhibitorProfile1();
                    expr1.USC = dmo.USC;
                    expr1.TheatreName = dmo.TheatreName;
                    expr1.Address = dmo.Address;
                    expr1.TheatreType = dmo.TheatreType;
                    expr1.ScreenCount = Convert.ToInt32(dmo.ScreenCount);
                    expr1.Email = dmo.Email;
                    expr1.ContactNumber = dmo.Mobile;
                    db.ExhibitorProfile1.Add(expr1);
                    db.SaveChanges();

                    string msgBody = "<p>We welcome you to Seats n Shows (SNS) a market place for cinema. <mark>Your Unique Screen Code or USC(<b>" + dmo.USC + "</b>) is an extremely important piece of  information for all your interactions with SNS.<mark></p>" +
                                            "<p>Please visit the following link and register.We would also like to advise you to complete your profile in order for us to better serve you.</p>" +
                                            "<br><p>Please contact us at help@sns.com for any questions of concerns</p>";
                    Utility.SendMail(msgBody, dmo.Email, "User Screen Code from S & S");
                    return this.RedirectToAction("AboutTheatre1", new { idQS = dmo.Id });
                }
            }
            catch (Exception ex)
            {
                return View();
            }
            return View(exhibitor);
        }

        // GET: Customers/Edit/5
        public ActionResult Edit(int id)
        {

            string category = "";
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Customers exhibitor = db.Customers.Find(id);
            var dmo = Mapper.Map<Customers, CustomersVM>(exhibitor);
            if (Request.QueryString["Category"] != null)
            {
                category = Request.QueryString["Category"];
            }
            ViewBag.Category = category;
            dmo.Category = category;
            if (dmo == null)
            {
                return HttpNotFound();
            }
            return View("Create", dmo);
        }

        // POST: Customers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TheatreName,Address,Country,State,City,Zip,TheatreType,ScreenCount,TicketPrice,Email,Mobile")] CustomersVM exhibitor)
        {
            if (ModelState.IsValid)
            {
                var dmo = Mapper.Map<CustomersVM, Customers>(exhibitor);
                db.Entry(dmo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Customer", "Home");
            }
            return View(exhibitor);
        }

        // GET: Customers/Delete/5
        public ActionResult Delete()
        {
            int id = Convert.ToInt32(Request.QueryString["idQS"]);
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customers exhibitor = db.Customers.Find(id);
            if (exhibitor == null)
            {
                return HttpNotFound();
            }
            else
            {
                try { 
                db.Customers.Remove(exhibitor);
                db.SaveChanges();
                }catch(Exception ex)
                {
                    return View("Dashboard","Home");
                }
            }
            return RedirectToAction("Customer", "Home");
        }

        // POST: Customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Customers exhibitor = db.Customers.Find(id);
            db.Customers.Remove(exhibitor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult AboutTheatre1()
        {
            ExhibitorProfile1 exPr1 = new ExhibitorProfile1();
            string txtUSC = Request.QueryString["usc"];
            if(txtUSC == null)
            {
                int idValue = Convert.ToInt32(Request.QueryString["idQS"]);
                txtUSC = db.Customers.Where(i => i.Id == idValue).Single().USC;
            }
            exPr1.USC = txtUSC;
            try
            {
                exPr1 = db.ExhibitorProfile1.Where(i => i.USC == txtUSC).Single();
                return View(exPr1);
            }
            catch (Exception ex)
            {
                exPr1.USC = txtUSC;
                return View(exPr1);
            }

            //exPr1VM.Id = exPr1.Id;
            //exPr1VM.TheatreName = exPr1.TheatreName;
            //return View(exPr1);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AboutTheatre1([Bind(Include = "Id,USC,TheatreName,Address,TheatreType,ScreenCount,StaffStrength,FandbBusiness,OtherAmenities,ContactNumber,Email,OwnershipProprietary,OwnershipPartnership,OwnershipCompany")] ExhibitorProfile1VM exPr1VM)
        {
            try
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);

                if (ModelState.IsValid)
                {
                    var dmo = Mapper.Map<ExhibitorProfile1VM, ExhibitorProfile1>(exPr1VM);
                    if (dmo.Id == 0)
                    {
                        db.ExhibitorProfile1.Add(dmo);
                    }
                    else
                    {
                        db.Entry(dmo).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    return this.RedirectToAction("AboutTheatre2", new { usc = dmo.USC });
                }
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return View();
            }
            return View();
        }

        public ActionResult AboutTheatre2()
        {
            string uscValue = Request.QueryString["usc"];
            ExhibitorProfile2 exPr2 = new ExhibitorProfile2();

            string textUSC = uscValue;

            try
            {
                exPr2 = db.ExhibitorProfile2.Where(i => i.USC == textUSC).Single();
                return View("AboutTheatre2", exPr2);
            }
            catch (Exception ex)
            {
                exPr2.USC = textUSC;
                return View("AboutTheatre2", exPr2);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AboutTheatre2([Bind(Include = "Id,USC,TotalCapacity,StallCapacity,StallPrice,UstallCapacity,UstallPrice,DresscircleCapacity,DresscirclePrice,BalconyCapacity,BalconyPrice,BoxCapacity,BoxPrice,SpleventPrice,AvgpriceRealization,WalkinVsAdvancePercent,WalkinVsAdvanceComments,AnnualAVgOccupancy,OccupancyforBreakevenShows,MinRentExpectations")] ExhibitorProfile2VM exPr2VM)
        {
            try
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);

                if (ModelState.IsValid)
                {
                    var dmo2 = Mapper.Map<ExhibitorProfile2VM, ExhibitorProfile2>(exPr2VM);

                    if (dmo2.Id == 0)
                    {
                        db.ExhibitorProfile2.Add(dmo2);
                    }
                    else
                    {
                        db.Entry(dmo2).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    return this.RedirectToAction("AboutTheatre3", new { usc = dmo2.USC });
                }
            }
            catch (Exception ex)
            {
                return View();
            }
            return View();
        }

        public ActionResult AboutTheatre3()
        {
            string uscValue = Request.QueryString["usc"];
            ExhibitorProfile3 exPr3 = new ExhibitorProfile3();

            string textUSC = uscValue;

            try
            {
                exPr3 = db.ExhibitorProfile3.Where(i => i.USC == textUSC).Single();
                return View("AboutTheatre3", exPr3);
            }
            catch (Exception ex)
            {
                exPr3.USC = textUSC;
                return View("AboutTheatre3", exPr3);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AboutTheatre3([Bind(Include = "Id,USC,FandbRevenue,ParkingRevenue,LocalAdvertisingRevenue,OtherRevenue,FandbBusiness,AdditionalRevenue,EffectiveTaxRate,Avg3YrRevenue")] ExhibitorProfile3VM exPr3VM)
        {
            try
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);

                if (ModelState.IsValid)
                {
                    var dmo3 = Mapper.Map<ExhibitorProfile3VM, ExhibitorProfile3>(exPr3VM);

                    if (dmo3.Id == 0)
                    {
                        db.ExhibitorProfile3.Add(dmo3);
                    }
                    else
                    {
                        db.Entry(dmo3).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    return this.RedirectToAction("AboutTheatre4", new { usc = dmo3.USC });
                }
            }
            catch (Exception ex)
            {
                return View();
            }
            return View();
        }
        public ActionResult AboutTheatre4()
        {
            string uscValue = Request.QueryString["usc"];
            ExhibitorProfile4 exPr4 = new ExhibitorProfile4();

            string textUSC = uscValue;

            try
            {
                exPr4 = db.ExhibitorProfile4.Where(i => i.USC == textUSC).Single();
                return View("AboutTheatre4", exPr4);
            }
            catch (Exception ex)
            {
                exPr4.USC = textUSC;
                return View("AboutTheatre4", exPr4);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AboutTheatre4([Bind(Include = "Id,USC,DigitalIntrest,DigitalComments,DigitalCompany,BackEnd,BookingSystem,ScreeningSystemAnalogue,ScreeningSystemDigital,PaymentTypeCashOnly,PaymentTypeAcceptcard,PaymentTypeOnline")] ExhibitorProfile4VM exPr4VM)
        {
            try
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);

                if (ModelState.IsValid)
                {
                    var dmo4 = Mapper.Map<ExhibitorProfile4VM, ExhibitorProfile4>(exPr4VM);

                    if (dmo4.Id == 0)
                    {
                        db.ExhibitorProfile4.Add(dmo4);
                    }
                    else
                    {
                        db.Entry(dmo4).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    return this.RedirectToAction("AboutTheatre5", new { usc = dmo4.USC });
                }
            }
            catch (Exception ex)
            {
                return View();
            }
            return View();
        }
        public ActionResult AboutTheatre5()
        {
            string uscValue = Request.QueryString["usc"];
            ExhibitorProfile5 exPr5 = new ExhibitorProfile5();

            string textUSC = uscValue;

            try
            {
                exPr5 = db.ExhibitorProfile5.Where(i => i.USC == textUSC).Single();
                return View("AboutTheatre5", exPr5);
            }
            catch (Exception ex)
            {
                exPr5.USC = textUSC;
                return View("AboutTheatre5", exPr5);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AboutTheatre5([Bind(Include = "Id,USC,Genres,BestReleaseFrom,BestReleaseTo,WorstReleaseFrom,WorstReleaseTo,SuccessfulStars,RegionalInterest,RegionalProfitability,RegionalDistribution,EnglishInterest,EnglishProfitability,EnglishDistribution,DubbedInterest,DubbedProfitability,DubbedDistribution,UniqueCase,UniqueFlop,BusinessExistingChallenges,BusinessPotentialSolutions,FutureExistingChallenges,FuturePotentialSolutions")] ExhibitorProfile5VM exPr5VM)
        {
            try
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);

                if (ModelState.IsValid)
                {
                    var dmo5 = Mapper.Map<ExhibitorProfile5VM, ExhibitorProfile5>(exPr5VM);

                    if (dmo5.Id == 0)
                    {
                        db.ExhibitorProfile5.Add(dmo5);
                    }
                    else
                    {
                        db.Entry(dmo5).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    return this.RedirectToAction("AboutTheatre6", new { usc = dmo5.USC });
                }
            }
            catch (Exception ex)
            {
                return View();
            }
            return View();
        }
        public ActionResult AboutTheatre6()
        {
            string uscValue = Request.QueryString["usc"];
            ExhibitorProfile6 exPr6 = new ExhibitorProfile6();

            string textUSC = uscValue;

            try
            {
                exPr6 = db.ExhibitorProfile6.Where(i => i.USC == textUSC).Single();
                return View("AboutTheatre6", exPr6);
            }
            catch (Exception ex)
            {
                exPr6.USC = textUSC;
                return View("AboutTheatre6", exPr6);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AboutTheatre6([Bind(Include = "Id,USC,PublicityExpenses,PublicityProperty,PosterSourcing,PosterCostWhom,DistributorChallenges,Distributorpractises,ProducerChallenges,ProducerDeals,OccupancyChallenges,OccupancySupport")] ExhibitorProfile6VM exPr6VM)
        {
            try
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);

                if (ModelState.IsValid)
                {
                    var dmo6 = Mapper.Map<ExhibitorProfile6VM, ExhibitorProfile6>(exPr6VM);

                    if (dmo6.Id == 0)
                    {
                        db.ExhibitorProfile6.Add(dmo6);
                    }
                    else
                    {
                        db.Entry(dmo6).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    return RedirectToAction("Dashboard","Home");
                }
            }
            catch (Exception ex)
            {
                return View();
            }
            return View();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}