﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SNS.Models;

namespace SNS.ViewModels
{
    public class ExhibitorProfile3VM
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "*")]
        [DisplayName("USC : ")]
        public string USC { get; set; }

        [DisplayName("F & B Revenue : ")]
        public string FandbRevenue { get; set; }

        [DisplayName("Parking Revenue : ")]
        public string ParkingRevenue { get; set; }

        [DisplayName("Local Advertising Revenue : ")]
        public string LocalAdvertisingRevenue { get; set; }

        [DisplayName("Other Revenue : ")]
        public string OtherRevenue { get; set; }

        [DisplayName("F & B Business : ")]
        public string FandbBusiness { get; set; }

        [DisplayName("Additional Revenue : ")]
        public string AdditionalRevenue { get; set; }

        [DisplayName("Effective Tax Rate : ")]
        public string EffectiveTaxRate { get; set; }

        [DisplayName("Average 3 Years Revenue : ")]
        public string Avg3YrRevenue { get; set; }
        
    }
}