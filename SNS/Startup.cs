﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using SNS.Models;
using Owin;
using System.Security.Claims;

[assembly: OwinStartupAttribute(typeof(SNS.Startup))]
namespace SNS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
			createRolesandUsers();
		}


		// In this method we will create default User roles and Admin user for login
		private void createRolesandUsers()
		{
			ApplicationDbContext context = new ApplicationDbContext();

			var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
			var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));


			// In Startup iam creating first Admin Role and creating a default Admin User 
			if (!roleManager.RoleExists("Admin"))
            {

                // first we create Admin rool
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);

                //Here we create a Admin super user who will maintain the website				

                AddUser(UserManager,"Admin","123456","Admin");
            }

            // creating Creating Manager role 
            if (!roleManager.RoleExists("Salesagent"))
			{
				var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
				role.Name = "Salesagent";
				roleManager.Create(role);
                AddUser(UserManager, "Salesagent", "Salesagent", "Salesagent");
            }

            // creating Creating Employee role 
            if (!roleManager.RoleExists("Customer"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Customer";
                roleManager.Create(role);
                AddUser(UserManager, "Customer", "Customer", "Customer");
            }

            // creating Exhibitor role 
            if (!roleManager.RoleExists("Customer"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Exhibitor";
                roleManager.Create(role);
                AddUser(UserManager, "Exhibitor", "Exhibitor", "Exhibitor");
            }
        }

        private static void AddUser(UserManager<ApplicationUser> UserManager,string userName,string pwd,string role)
        {
            var user = new ApplicationUser();
            user.UserName = userName;
            user.Email = userName+"@gmail.com";
            string userPWD = pwd;
            var chkUser = UserManager.Create(user, userPWD);
            //Add default Users
            if (chkUser.Succeeded)
            {
                var result1 = UserManager.AddToRole(user.Id, role);

            }
        }
    }
}
