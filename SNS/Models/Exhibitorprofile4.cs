﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SNS.Models
{
    public class ExhibitorProfile4

    {
        public int Id { get; set; }
        public string USC { get; set; }

        public bool ScreeningSystemAnalogue { get; set; }
        public bool ScreeningSystemDigital { get; set; }

        public bool DigitalInterest { get; set; }

        public string DigitalComments { get; set; }

        public string DigitalCompany { get; set; }

        public string BackEnd { get; set; }

        public string BookingSystem { get; set; }

        public bool PaymentTypeCashOnly { get; set; }
        public bool PaymentTypeAcceptcard { get; set; }
        public bool PaymentTypeOnline { get; set; }


    }
}