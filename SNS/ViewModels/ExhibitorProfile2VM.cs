﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SNS.Models;

namespace SNS.ViewModels
{
    public class ExhibitorProfile2VM
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "*")]
        [DisplayName("USC : ")]
        public string USC { get; set; }

        [DisplayName("Total Capacity : ")]
        public int TotalCapacity { get; set; }

        [DisplayName("Stall Capacity : ")]
        public int StallCapacity { get; set; }

        [DisplayName("Stall Price : ")]
        public string StallPrice { get; set; }

        [DisplayName("Ustall Capacity : ")]
        public int UstallCapacity { get; set; }

        [DisplayName("Ustall Price : ")]
        public string UstallPrice { get; set; }

        [DisplayName("Dress Circle Capacity : ")]
        public int DresscircleCapacity { get; set; }

        [DisplayName("Dress Circle Price : ")]
        public string DresscirclePrice { get; set; }

        [DisplayName("Balcony Capacity : ")]
        public int BalconyCapacity { get; set; }

        [DisplayName("Balcony Price : ")]
        public string BalconyPrice { get; set; }

        [DisplayName("Box Capacity : ")]
        public int BoxCapacity { get; set; }

        [DisplayName("Box Price : ")]
        public string BoxPrice { get; set; }

        [DisplayName("Spl Event Price : ")]
        public string SpleventPrice { get; set; }

        [DisplayName("Avg Price Realization : ")]
        public string AvgpriceRealization { get; set; }

        [DisplayName("WalkinVsAdvancePercent : ")]
        public string WalkinVsAdvancePercent { get; set; }

        [DisplayName("WalkinVsAdvanceComments : ")]
        public string WalkinVsAdvanceComments { get; set; }

        [DisplayName("Annual Average Occupancy : ")]
        public string AnnualAVgOccupancy { get; set; }

        [DisplayName("Occupancy for BreakevenShows : ")]
        public string OccupancyforBreakevenShows { get; set; }

        [DisplayName("Minimum Rent Expectations : ")]
        public string MinRentExpectations { get; set; }





    }
}