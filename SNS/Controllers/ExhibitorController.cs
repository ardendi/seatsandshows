﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using SNS.Models;
using SNS.ViewModels;
namespace SNS.Controllers
{
public class ExhibitorController: Controller
    {
        ApplicationDbContext db;
        ApplicationDbContext context;
        public ExhibitorController()
        {
            db = new ApplicationDbContext();
            context = new ApplicationDbContext();
        }
      
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveExhibitorProfiles1([Bind(Include = "USC,TheatreName,Address,TheatreType,ScreenCount,StaffStrength,FandbBusiness,OtherAmenities,Ownership,ContactNumber,Email")] ExhibitorProfile1VM exPr1)
        {
            try
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                if (ModelState.IsValid)
                {
                    var dmo = Mapper.Map<ExhibitorProfile1VM, ExhibitorProfile1>(exPr1);
                    //db.Entry(dmo).State = EntityState.Modified;
                    db.ExhibitorProfile1.Add(dmo);
                    db.SaveChanges();
                    return RedirectToAction("Index", "Exhibitor");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                return View();
            }

            return RedirectToAction("Index", "Customer");

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveExhibitorProfiles2([Bind(Include = "USC,TotalCapacity,StallCapacity,StallPrice,UstallCapacity,UstallPrice,DresscircleCapacity,DresscirclePrice,BalconyCapacity,BalconyPrice,BoxCapacity,BoxPrice,SpleventPrice,AvgpriceRalization,WalkinPercent,AdvancePercent,OccupancyforBreakevenShows,MinRentExpectations")] ExhibitorProfile2VM exPr2)
        {
            try
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);
                if (ModelState.IsValid)
                {
                    var dmo = Mapper.Map<ExhibitorProfile2VM, ExhibitorProfile2>(exPr2);
                    //db.Entry(dmo).State = EntityState.Modified;
                    db.ExhibitorProfile2.Add(dmo);
                    db.SaveChanges();
                    return RedirectToAction("Index", "Exhibitor");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
                return View();
            }

            return RedirectToAction("Index", "Customer");

        }

        public ActionResult LoadProfile(string usc)
        {
            try
            {
                if (usc.Equals(null))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                ExhibitorProfile1 exPr1 = db.ExhibitorProfile1.Where(i => i.USC == usc).Single();
                //var dmoExPr1 = Mapper.Map<ExhibitorProfile1, ExhibitorProfile1VM>(exPr1);
                if (exPr1 == null)
                {
                    return HttpNotFound();
                }
                return PartialView("_exhibitorProfilePopUp", exPr1);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return RedirectToAction(this.Request.UrlReferrer.AbsolutePath);
            }
        }

        public ActionResult Profile1()
        {
            string uscValue = Request.QueryString["usc"];
            ExhibitorProfile1 exPr1 = new ExhibitorProfile1();
            string txtUSC = db.Customers.Where(i => i.USC == uscValue).Single().USC;
            exPr1.USC = txtUSC;
            try
            {
                exPr1 = db.ExhibitorProfile1.Where(i => i.USC == txtUSC).Single();
                return View(exPr1);
            }
            catch (Exception ex)
            {
                exPr1.USC = txtUSC;
                return View(exPr1);
            }

            //exPr1VM.Id = exPr1.Id;
            //exPr1VM.TheatreName = exPr1.TheatreName;
            //return View(exPr1);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Profile1([Bind(Include = "Id,USC,TheatreName,Address,TheatreType,ScreenCount,StaffStrength,FandbBusiness,OtherAmenities,ContactNumber,Email,OwnershipProprietary,OwnershipPartnership,OwnershipCompany")] ExhibitorProfile1VM exPr1VM)
        {
            try
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);

                if (ModelState.IsValid)
                {
                    var dmo = Mapper.Map<ExhibitorProfile1VM, ExhibitorProfile1>(exPr1VM);

                    if (dmo.Id == 0)
                    {
                        db.ExhibitorProfile1.Add(dmo);
                    }
                    else
                    {
                        db.Entry(dmo).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    return this.RedirectToAction("Profile2", new { usc = dmo.USC });
                }
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
                return View();
            }
            return View();
        }
        public ActionResult Profile2()
        {
            string uscValue = Request.QueryString["usc"];
            ExhibitorProfile2 exPr2 = new ExhibitorProfile2();

            string textUSC = uscValue;

            try
            {
                exPr2 = db.ExhibitorProfile2.Where(i => i.USC == textUSC).Single();
                return View("Profile2", exPr2);
            }
            catch (Exception ex)
            {
                exPr2.USC = textUSC;
                return View("Profile2", exPr2);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Profile2([Bind(Include = "Id,USC,TotalCapacity,StallCapacity,StallPrice,UstallCapacity,UstallPrice,DresscircleCapacity,DresscirclePrice,BalconyCapacity,BalconyPrice,BoxCapacity,BoxPrice,SpleventPrice,AvgpriceRealization,WalkinVsAdvancePercent,WalkinVsAdvanceComments,AnnualAVgOccupancy,OccupancyforBreakevenShows,MinRentExpectations")] ExhibitorProfile2VM exPr2VM)
        {
            try
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);

                if (ModelState.IsValid)
                {
                    var dmo2 = Mapper.Map<ExhibitorProfile2VM, ExhibitorProfile2>(exPr2VM);

                    if (dmo2.Id == 0)
                    {
                        db.ExhibitorProfile2.Add(dmo2);
                    }
                    else
                    {
                        db.Entry(dmo2).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    return this.RedirectToAction("Profile3", new { usc = dmo2.USC });
                }
            }
            catch (Exception ex)
            {
                return View();
            }
            return View();
        }

        public ActionResult Profile3()
        {
            string uscValue = Request.QueryString["usc"];
            ExhibitorProfile3 exPr3 = new ExhibitorProfile3();

            string textUSC = uscValue;

            try
            {
                exPr3 = db.ExhibitorProfile3.Where(i => i.USC == textUSC).Single();
                return View("Profile3", exPr3);
            }
            catch (Exception ex)
            {
                exPr3.USC = textUSC;
                return View("Profile3", exPr3);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Profile3([Bind(Include = "Id,USC,FandbRevenue,ParkingRevenue,LocalAdvertisingRevenue,OtherRevenue,FandbBusiness,AdditionalRevenue,EffectiveTaxRate,Avg3YrRevenue")] ExhibitorProfile3VM exPr3VM)
        {
            try
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);

                if (ModelState.IsValid)
                {
                    var dmo3 = Mapper.Map<ExhibitorProfile3VM, ExhibitorProfile3>(exPr3VM);

                    if (dmo3.Id == 0)
                    {
                        db.ExhibitorProfile3.Add(dmo3);
                    }
                    else
                    {
                        db.Entry(dmo3).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    return this.RedirectToAction("Profile4", new { usc = dmo3.USC });
                }
            }
            catch (Exception ex)
            {
                return View();
            }
            return View();
        }
        public ActionResult Profile4()
        {
            string uscValue = Request.QueryString["usc"];
            ExhibitorProfile4 exPr4 = new ExhibitorProfile4();

            string textUSC = uscValue;

            try
            {
                exPr4 = db.ExhibitorProfile4.Where(i => i.USC == textUSC).Single();
                return View("Profile4", exPr4);
            }
            catch (Exception ex)
            {
                exPr4.USC = textUSC;
                return View("Profile4", exPr4);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Profile4([Bind(Include = "Id,USC,DigitalIntrest,DigitalComments,DigitalCompany,BackEnd,BookingSystem,ScreeningSystemAnalogue,ScreeningSystemDigital,PaymentTypeCashOnly,PaymentTypeAcceptcard,PaymentTypeOnline")] ExhibitorProfile4VM exPr4VM)
        {
            try
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);

                if (ModelState.IsValid)
                {
                    var dmo4 = Mapper.Map<ExhibitorProfile4VM, ExhibitorProfile4>(exPr4VM);

                    if (dmo4.Id == 0)
                    {
                        db.ExhibitorProfile4.Add(dmo4);
                    }
                    else
                    {
                        db.Entry(dmo4).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    return this.RedirectToAction("Profile5", new { usc = dmo4.USC });
                }
            }
            catch (Exception ex)
            {
                return View();
            }
            return View();
        }
        public ActionResult Profile5()
        {
            string uscValue = Request.QueryString["usc"];
            ExhibitorProfile5 exPr5 = new ExhibitorProfile5();

            string textUSC = uscValue;

            try
            {
                exPr5 = db.ExhibitorProfile5.Where(i => i.USC == textUSC).Single();
                return View("Profile5", exPr5);
            }
            catch (Exception ex)
            {
                exPr5.USC = textUSC;
                return View("Profile5", exPr5);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Profile5([Bind(Include = "Id,USC,Genres,BestReleaseFrom,BestReleaseTo,WorstReleaseFrom,WorstReleaseTo,SuccessfulStars,RegionalInterest,RegionalProfitability,RegionalDistribution,EnglishInterest,EnglishProfitability,EnglishDistribution,DubbedInterest,DubbedProfitability,DubbedDistribution,UniqueCase,UniqueFlop,BusinessExistingChallenges,BusinessPotentialSolutions,FutureExistingChallenges,FuturePotentialSolutions")] ExhibitorProfile5VM exPr5VM)
        {
            try
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);

                if (ModelState.IsValid)
                {
                    var dmo5 = Mapper.Map<ExhibitorProfile5VM, ExhibitorProfile5>(exPr5VM);

                    if (dmo5.Id == 0)
                    {
                        db.ExhibitorProfile5.Add(dmo5);
                    }
                    else
                    {
                        db.Entry(dmo5).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    return this.RedirectToAction("Profile6", new { usc = dmo5.USC });
                }
            }
            catch (Exception ex)
            {
                return View();
            }
            return View();
        }
        public ActionResult Profile6()
        {
            string uscValue = Request.QueryString["usc"];
            ExhibitorProfile6 exPr6 = new ExhibitorProfile6();

            string textUSC = uscValue;

            try
            {
                exPr6 = db.ExhibitorProfile6.Where(i => i.USC == textUSC).Single();
                return View("Profile6", exPr6);
            }
            catch (Exception ex)
            {
                exPr6.USC = textUSC;
                return View("Profile6", exPr6);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Profile6([Bind(Include = "Id,USC,PublicityExpenses,PublicityProperty,PosterSourcing,PosterCostWhom,DistributorChallenges,Distributorpractises,ProducerChallenges,ProducerDeals,OccupancyChallenges,OccupancySupport")] ExhibitorProfile6VM exPr6VM)
        {
            try
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors);

                if (ModelState.IsValid)
                {
                    var dmo6 = Mapper.Map<ExhibitorProfile6VM, ExhibitorProfile6>(exPr6VM);

                    if (dmo6.Id == 0)
                    {
                        db.ExhibitorProfile6.Add(dmo6);
                    }
                    else
                    {
                        db.Entry(dmo6).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    return RedirectToAction("Index", "Exhibitor");
                }
            }
            catch (Exception ex)
            {
                return View();
            }
            return View();
        }

    }
}