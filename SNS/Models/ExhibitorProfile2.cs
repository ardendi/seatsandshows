﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SNS.Models
{
    public class ExhibitorProfile2
    {

        public int Id { get; set; }
        public string USC { get; set; }
        public int TotalCapacity { get; set; }
        public int StallCapacity { get; set; }
        public string StallPrice { get; set; }
        public int UstallCapacity { get; set; }
        public string UstallPrice { get; set; }
        public int DresscircleCapacity { get; set; }
        public string DresscirclePrice { get; set; }
        public int BalconyCapacity { get; set; }
        public string BalconyPrice { get; set; }
        public int BoxCapacity { get; set; }
        public string BoxPrice { get; set; }
        public string SpleventPrice { get; set; }
        public string AvgpriceRealization { get; set; }
        public string WalkinVsAdvancePercent { get; set; }
        public string WalkinVsAdvanceComments { get; set; }
        public string AnnualAVgOccupancy { get; set; }
        public string OccupancyforBreakevenShows { get; set; }
        public string MinRentExpectations { get; set; }


    }
}