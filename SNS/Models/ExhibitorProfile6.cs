﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SNS.Models
{
    public class ExhibitorProfile6

    {
        public int Id { get; set; }
        public string USC { get; set; }
        public string PublicityExpenses { get; set; }
        public string PublicityProperty { get; set; }
        public string PosterSourcing { get; set; }
        public string PosterCostWhom { get; set; }
        public string DistributorChallenges { get; set; }
        public string Distributorpractises { get; set; }
        public string ProducerChallenges { get; set; }
        public string ProducerDeals { get; set; }
        public string OccupancyChallenges { get; set; }
        public string OccupancySupport { get; set; }
       

    }
}