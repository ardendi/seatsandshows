﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;

namespace SNS.Common
{
    public static class Utility
    {
        public static void SendMail(string messageBody,string to,string subject)
        {
            MailMessage mail = new MailMessage("admin@example.com", to);
            mail.Subject = subject;
            mail.Body = messageBody;
            mail.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            
            smtp.Credentials = new NetworkCredential(
                WebConfigurationManager.AppSettings["SMTPEmail"].ToString(), WebConfigurationManager.AppSettings["Password"].ToString());
            smtp.EnableSsl = true;           
            smtp.Send(mail);
        }

        public static string GenerateOTP()
        {
            /* 6-DIGT OTP */
            string otp = "";
            Random random = new Random();
            for(int i=0;i<6;i++)
                otp += random.Next(1, 10);

            return otp;

            /* STRING OTP */
            //string alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            //string small_alphabets = "abcdefghijklmnopqrstuvwxyz";
            //string numbers = "1234567890";

            //string characters = numbers;

            //characters += alphabets + small_alphabets + numbers;

            //int length = 7;
            //string otp = string.Empty;
            //for (int i = 0; i < length; i++)
            //{
            //    string character = string.Empty;
            //    do
            //    {
            //        int index = new Random().Next(0, characters.Length);
            //        character = characters.ToCharArray()[index].ToString();
            //    } while (otp.IndexOf(character) != -1);
            //    otp += character;
            //}
            //// lblOTP.Text = otp;
            //return otp;
        }

        public static string GetThreeDigitCode(string str)
        {
            if (str.Length > 2)
            {
                return str.Substring(0,3).ToUpper();
            }
            else
            {
                return str.ToUpper();
            }
        }
    }
}