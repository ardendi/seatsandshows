﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using SNS.Models;

namespace SNS.ViewModels
{
    public class ExhibitorProfile4VM

    {
        public int Id { get; set; }

        [Required(ErrorMessage = "*")]
        [DisplayName("USC : ")]
        public string USC { get; set; }

        [DisplayName("Screening System Analogue : ")]
        public bool ScreeningSystemAnalogue { get; set; }
        [DisplayName("Screening System Digital : ")]
        public bool ScreeningSystemDigital { get; set; }

        [DisplayName("Digital Interest : ")]
        public bool DigitalInterest { get; set; }

        [DisplayName("Digital Comments : ")]
        public string DigitalComments { get; set; }

        [DisplayName("Digital Company : ")]
        public string DigitalCompany { get; set; }

        [DisplayName("Back End : ")]
        public string BackEnd { get; set; }

        [DisplayName("Booking System : ")]
        public string BookingSystem { get; set; }

        [DisplayName("Payment Type Cash Only : ")]
        public bool PaymentTypeCashOnly { get; set; }
        [DisplayName("Payment Type Accept card : ")]
        public bool PaymentTypeAcceptcard { get; set; }
        [DisplayName("Payment Type Online : ")]
        public bool PaymentTypeOnline { get; set; }

    }
}